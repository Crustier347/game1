using System;
using UnityEngine.InputSystem;
using UnityEngine;

[CreateAssetMenu(fileName = "Inputs", menuName = "ScriptableObjects/Input/InputReader")]
public class InputReader : ScriptableObject
{
    private GameControls gameControls;

    private void OnEnable()
    {
        gameControls ??= new GameControls();
        gameControls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        gameControls.Gameplay.Disable();
    }

    public float GetMoveValue()
    {
        return gameControls.Gameplay.Move.ReadValue<float>();
    }

    public event Action<InputAction.CallbackContext> Jump
    {
        add => gameControls.Gameplay.Jump.performed += value;
        remove => gameControls.Gameplay.Jump.performed -= value;
    }
}
