using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    public static event Action NoLivesAction;
    public static event Action<int> LivesChangedAction;
    public static CheckpointManager Instance { get; private set; }
    [SerializeField] private int livesLeft;
    private Checkpoint currentCheckpoint;
    private GameObject player;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void SetCurrentCheckpoint(Checkpoint checkpointToSet)
    {
        currentCheckpoint = checkpointToSet;
        Debug.Log(currentCheckpoint.gameObject.name);
    }

    public void Respawn()
    {
        if(livesLeft > 1)
        {
            player.transform.position = currentCheckpoint.transform.position;
            livesLeft--;
            LivesChangedAction.Invoke(livesLeft);
        }
        else
        {
            NoLivesAction?.Invoke();
        }
    }
}