using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float playerSpeed;
    [SerializeField] private float playerJumpForce;
    [SerializeField] private LayerMask platformLayerMask;
    [SerializeField] InputReader inputReader;

    private Rigidbody2D rb;
    private BoxCollider2D playerCollider;
    private FallDeathCheckPlayer fallDeathCheck;
    private Animator animator;

    private Vector2 moveDirection;
    private Vector2 jumpVector;
    private float playerX;
    private bool canDoubleJump;
    private bool isFacingRight = true;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<BoxCollider2D>();
        fallDeathCheck = GetComponent<FallDeathCheckPlayer>();
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        inputReader.Jump += HandleJump;
    }

    private void OnDisable()
    {
        inputReader.Jump -= HandleJump;
    }

    private void Update()
    {
        HandleMovement();
        CheckForFalling();
        IsGrounded(0.09f);
    }


    private void FixedUpdate()
    {
        rb.velocity = moveDirection;
    }
    public bool IsGrounded(float checkDistance)
    {
        //to co� m�wi czy jest collider:)

        RaycastHit2D groundHit = Physics2D.BoxCast(playerCollider.bounds.center, playerCollider.bounds.size, 0f, Vector2.down, checkDistance, platformLayerMask);

        if (groundHit.collider != null)
        {
            canDoubleJump = true;
            fallDeathCheck.ResetDeathTimer();
            if (rb.velocity.y <= 0f)
            {
                StopFallingAnim();
            } 
            return true;
        }
        return false;
    }

    private void HandleJump(InputAction.CallbackContext callbackContext)
    {
        if (!IsGrounded(0.09f) && canDoubleJump == false)
        {
            return;
        }
        Jump();
    }

    private void HandleMovement()
    {
        playerX = inputReader.GetMoveValue();
        SetRunningAnim();
        Flip();
        moveDirection.x = playerX * playerSpeed;
        moveDirection.y = rb.velocity.y;
    }

    private void Flip()
    {
        if (isFacingRight == true && playerX < 0)
        {
            isFacingRight = false;
            transform.localEulerAngles = new Vector3(0f,180f,0f);
        }
        else if(isFacingRight == false && playerX > 0)
        {
            isFacingRight = true;
            transform.localEulerAngles = Vector3.zero;
        }
    }

    //skok
    private void Jump()
    {
        jumpVector.x = rb.velocity.x;
        jumpVector.y = playerJumpForce;
        rb.velocity = jumpVector;
        StartJumpingAnim();
        if(!IsGrounded(0.09f))
        {
            canDoubleJump = false;
        }
    }


    #region Animations
    private void SetRunningAnim()
    {
        if (playerX != 0f)
        {
            animator.SetBool("IsRunning", true);
        }
        else
        {
            animator.SetBool("IsRunning", false);
        }
    }

    private void StartJumpingAnim()
    {
        animator.SetBool("IsJumping", true);
    }

    private void StartFallingAnim()
    {
        animator.SetBool("IsJumping", false);
        animator.SetBool("IsFalling", true);
    }

    private void StopFallingAnim()
    {
        animator.SetBool("IsFalling", false);
    }

    private void CheckForFalling()
    {
        if (rb.velocity.y < 0f)
        {
            StartFallingAnim();
        }
    }
    #endregion
}
