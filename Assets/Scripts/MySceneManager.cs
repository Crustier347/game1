using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{
    private void OnEnable()
    {
        CheckpointManager.NoLivesAction += OnNoLivesAction;
    }

    private void OnDisable()
    {
        CheckpointManager.NoLivesAction -= OnNoLivesAction;
    }
    private void OnNoLivesAction()
    {
        RestartLevel();
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
