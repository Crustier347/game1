using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class FallDeathCheckPlayer : MonoBehaviour
{
    [SerializeField] private float deathTimer = 3f;
    [SerializeField] private float fallDistance;
    private float deathTimerMax;
    private float startY;
    private PlayerMovement playerMovement;

    private void Awake()
    {
        startY = transform.position.y;
        deathTimerMax = deathTimer;
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        if (!(startY - fallDistance > transform.position.y))
        {
            return;
        }

        if (playerMovement.IsGrounded(100f))
        {
            return;
        }

        deathTimer -= Time.deltaTime;
        if (deathTimer > 0)
        {
            return;
        }
        CheckpointManager.Instance.Respawn();
    }
    public void ResetDeathTimer()
    {
        deathTimer = deathTimerMax;
    }

}
