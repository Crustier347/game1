using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private CheckpointManager checkpointManager;
    private void Awake()
    {
        checkpointManager = GetComponentInParent<CheckpointManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            checkpointManager.SetCurrentCheckpoint(this);
        }
    }
}
