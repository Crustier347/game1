using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerGameObject1 : MonoBehaviour
{
    [SerializeField] private GameObject ObjectToToggle;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            if (ObjectToToggle.activeSelf)
            {
                ObjectToToggle.SetActive(false);
            }
            else
            {
                ObjectToToggle.SetActive(true);
            }
        }
    }
}
