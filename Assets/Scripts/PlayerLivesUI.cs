using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLivesUI : MonoBehaviour
{
    [SerializeField] private List<GameObject> livesImages;

    private void OnEnable()
    {
        CheckpointManager.LivesChangedAction += OnLivesChangedAction;
    }

    private void OnDisable()
    {
        CheckpointManager.LivesChangedAction -= OnLivesChangedAction;
    }

    private void OnLivesChangedAction(int livesLeft)
    {
        livesImages[livesLeft].SetActive(false);
    }
}
