using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        bool result = collision.gameObject.CompareTag("Player");
        if (result)
        {
            CheckpointManager.Instance.Respawn();
        }

    }
}
