using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomEnemy : MonoBehaviour
{
    [SerializeField] private bool isFacingRight;
    [SerializeField] private Transform[] waypoints;
    [SerializeField] private float enemySpeed;
    private Rigidbody2D rb;
    private Animator animator;
    private Transform currentPoint;
    private int index;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        currentPoint = waypoints[0];
        index = 0;
    }

    private void Update()
    {
        Debug.Log(transform.position.x - currentPoint.position.x);
        if (transform.position.x - currentPoint.position.x < 1f)
        {
            index++;
            if (index >= waypoints.Length)
            {
                currentPoint = waypoints[0];
            }
            else
            {
                currentPoint = waypoints[index];
            }
        }

    }

    private void FixedUpdate()
    {
        Vector2 moveDirection = currentPoint.position - transform.position;
        if (moveDirection.x > 0)
        {
            rb.velocity = new Vector2(enemySpeed, 0);
        }
        else
        {
            rb.velocity = new Vector2(-enemySpeed, 0);
        }
    }

    private void Flip()
    {
        if (isFacingRight == true)
        {
            isFacingRight = false;
            transform.localEulerAngles = new Vector3(0f, 180f, 0f);
        }
        else if (isFacingRight == false)
        {
            isFacingRight = true;
            transform.localEulerAngles = Vector3.zero;
        }
    }
}
